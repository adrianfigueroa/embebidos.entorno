#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "unt.h"
#include "soc.h"
#include "led.h"

SemaphoreHandle_t mutex;

void Azul(void * parametros) {
   while(1) {
    //Tomo el mutex 
    xSemaphoreTake(mutex, portMAX_DELAY);
    Led_On(RGB_B_LED);
    vTaskDelay(pdMS_TO_TICKS(1000));
    //Devuelvo el mutex
    xSemaphoreGive(mutex);
    Led_Off(RGB_B_LED);
    vTaskDelay(pdMS_TO_TICKS(1000));
   }
}

void Rojo(void * parametros) {
   while(1) {
    //Tomo el mutex 
    xSemaphoreTake(mutex, portMAX_DELAY);
    Led_On(RGB_R_LED);
    vTaskDelay(pdMS_TO_TICKS(1500));
    //Devuelvo el mutex
    xSemaphoreGive(mutex);
    Led_Off(RGB_R_LED);
    vTaskDelay(pdMS_TO_TICKS(1500));
   }
}

int main(void) {
   /* Inicializaciones y configuraciones de dispositivos */
   SisTick_Init();
   Init_Leds();

   mutex = xSemaphoreCreateMutex();
  

   /* Creación de las tareas */
   xTaskCreate(Azul, "Azul", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
   xTaskCreate(Rojo, "Rojo", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);


   /* Arranque del sistema operativo */
   vTaskStartScheduler();

   /* vTaskStartScheduler solo retorna si se detiene el sistema operativo */
   while(1);

   /* El valor de retorno es solo para evitar errores en el compilador*/
   return 0;
}