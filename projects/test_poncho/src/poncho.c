#include "unt.h"

void SysTick_Handler(void){
    static int divisor = 0;

    divisor = (divisor + 1) % 10;
    if (divisor == 0) {
      refrescarDigitos();
   }
}

int main(void){

    /*Inicializaciones y configuraciones de dispositivos*/

    Init_PonchoUNT();
    SisTick_Init();

    Test_PonchoUNT();

    return 0;
}